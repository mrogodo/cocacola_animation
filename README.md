# Animation Development Brief
A web page developed with SCSS (compiled to CSS), javascript & HTML5, which reflects the video animation scene.

 -  HTML – Clean semantic code
 -  SCSS - Usage of key frame animations, directives, variables and a grid system
 
 -   JavaScript / jQuery - function that activates animations
    -    If the function activate is executed the animations will begin.
    -    If the function deactivate is executed the animations will stop.
    -    If no click functionality enabled a looping animation is the expected behaviour.

## Content 
1920px width & 1080px height, with overflow: hidden style applied. Elements not be absolutely positioned, grid system applied. 